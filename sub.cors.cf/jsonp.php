<?php

include 'config.php';

if(isset($_GET['ns'])){
    header('X-Content-Type-Options: nosniff');
}

if(isset($_GET['cb'])){
    $cb = preg_replace('/[^a-z0-9_\.]/i', '', $_GET['cb']);
    echo $cb.'('.json_encode($secret).')';
}
else{
    echo json_encode($secret);
}