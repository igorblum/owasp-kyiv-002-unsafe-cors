<?php

if(isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
}

if(isset($_GET['creds'])) {
    header("Access-Control-Allow-Credentials: true");
}

include 'config.php';

echo json_encode($secret);