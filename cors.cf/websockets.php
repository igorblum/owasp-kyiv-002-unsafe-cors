<?php session_start();?>
<html>
    <head>
        <title>WebSocket Test</title>
    </head>
    <body>
    <form method="post">
        Name: <input type="text" name="name" value="<?php echo htmlentities(@$_COOKIE['name'])?>" /><br/>
        Secret: <input type="text" name="secret" value="<?php echo htmlentities(@$_COOKIE['secret'])?>"/><br/>
        <input type="submit" name="submit"/>
    </form>  

    <?php 

    if(isset($_POST['name'])&&isset($_POST['secret'])){
        setcookie("name", $_POST['name'], time()+3600);
        setcookie("secret", $_POST['secret'], time()+3600);
        echo '<script>location=location.href</script>';
    }
    if(isset($_COOKIE['name'])&&isset($_COOKIE['secret'])){ ?>
    <script>
            function init() { 
            websocket = new WebSocket("wss://cors.cf/soc/");
            websocket.onopen = function() { document.getElementById("output").innerHTML += "<p>> CONNECTED</p>"; };
            websocket.onmessage = function(evt) { document.getElementById("output").innerHTML += "<p style='color: blue;'>> RESPONSE: " + evt.data + "</p>"; };
            websocket.onerror = function(evt) { document.getElementById("output").innerHTML += "<p style='color: red;'>> ERROR: " + evt.data + "</p>"; };
            }
            function sendMessage(message) {
            document.getElementById("output").innerHTML += "<p>> SENT: " + message + "</p>";
            
            websocket.send(message);
            }
            
            window.addEventListener("load", init, false);
        </script>
        <input onkeypress="if(this.value) {if (window.event.keyCode == 13) { sendMessage(this.value); this.value = null; }}"/> press enter to post message
        <div id="output"></div>
    <?php
    }
    ?>
    </body>
</html>