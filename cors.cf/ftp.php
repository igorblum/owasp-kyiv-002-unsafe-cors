<?php

$origin = isset($_SERVER['HTTP_ORIGIN'])?$_SERVER['HTTP_ORIGIN']:'';

$url = parse_url($origin);

if(($url['scheme']=='http'||$url['scheme']=='https')&&$url['host']!='cors.cf') {
    die('Forbidden');
}

    
header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
header("Access-Control-Allow-Credentials: true");

include 'config.php';

echo json_encode($secret);